# puppeteer-pdf-report-server

A HTML to PDF report generator based on Puppeteer.
Run on standalone and Google Cloud Function.

# Using API
## Example of API request
```javascript
let htmlContent = '<p>hello world</p>';
let pdfOptions = {path: 'sample.pdf'}; // Optional

axios.post('http://localhost:7000', {
  htmlContent,
  pdfOptions
}).then(res => {
  // handle the pdf binary
});
```
## Example of PDF options
```javascript
{
  path: 'sample.pdf',
  format: 'A4',
  printBackground: true,
  margin: {
    top: '5mm',
    bottom: '5mm',
    right: '5mm',
    left: '5mm'
  }
}
```
Click the link below for more options detail information. 
https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions