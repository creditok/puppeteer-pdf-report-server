const puppeteer = require('puppeteer');

exports.pdfGenerator = async (req, res) => {

    const pdfBinary = await generatePdf(req.body.html, req.body.pdf);

    res.type('application/pdf');
    res.end(pdfBinary, 'binary');
};

async function generatePdf(htmlBody, pdfConfig) {
    const browser = await puppeteer.launch({args: ['--no-sandbox']});
    const page = await browser.newPage();
  
    await page.goto(`data:text/html,${htmlBody}`, { waitUntil: 'networkidle0' } );
    const pdfBinary = await page.pdf(pdfConfig);
   
    await browser.close();

    return pdfBinary;
}